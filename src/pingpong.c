/* Copyright 2020 Nico Sonack
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "pingpong.h"

#include <assert.h>

#include <SDL/SDL_gfxPrimitives.h>

#define PEDAL_WIDTH 50
#define PEDAL_HEIGHT 10

// REEeee double evaluation REEeee
#define min(x, y) (x) < (y) ? (x) : (y)
#define max(x, y) (x) > (y) ? (x) : (y)

#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof(arr[0]))

static
const char* game_over_messages[] =
    { "Game over. PepeHands",
      "Press ENTER to restart the game",
      "or ESC to exit."
    };

static
const char* start_screen_messages[] =
    { "PING PONG",
      "Press ENTER to start the game",
      "or ESC to exit."
    };

void game_initialize(PingPongGame*);

PingPongGame *create_game() {
    PingPongGame *game = malloc(sizeof(PingPongGame));
    if (game == NULL)
        return NULL;

    game_initialize(game);
    game->state = PP_STATE_START;

    return game;
}

void game_initialize(PingPongGame *game) {
    int random_num = rand(),
        spawn_x = 0;
    assert(game);

    spawn_x = random_num % (SCREEN_WIDTH - PEDAL_WIDTH);

    game->ball = (SDL_Rect){ .x = spawn_x, .y = 0, .w = 10, .h = 10 };
    game->pedal = (SDL_Rect){ .x = (SCREEN_WIDTH - PEDAL_WIDTH) / 2,
                    .y = SCREEN_HEIGHT - 13 - PEDAL_HEIGHT,
                    .h = PEDAL_HEIGHT,
                    .w = PEDAL_WIDTH };
    game->vel_x = (rand() % 2) ? 1 : -1;
    game->vel_y = 1;
    game->score = 0;
}

void destroy_game(PingPongGame *game) {
    if (game)
        free(game);
}

// TODO(#2): integrating the velocity over time does not take the actual dt into account.
int update_game(PingPongGame *game, Uint32 dt) {
    (void) dt;

    switch (game->state) {
    case PP_STATE_START:
    case PP_STATE_GAME_OVER: {
    } break;
    case PP_STATE_RUNNING: {
        // Collision checks
        if ((game->pedal.x <= game->ball.x &&
             game->ball.x <= game->pedal.x + game->pedal.w) &&
            (game->pedal.y <= game->ball.y &&
             game->ball.y <= game->pedal.y + game->pedal.h)) {
            game->vel_y *= -1;

            // Snap the ball
            if (game->vel_y < 0) {
                game->ball.y = game->pedal.y - game->ball.h;
            } else {
                game->ball.y = game->pedal.y + game->pedal.h;
            }

            game->score += 1;
        }

        // Integrating the velocity
        game->ball.x += game->vel_x;
        game->ball.y += game->vel_y;

        // Screen bound checking
        if ((game->ball.x <= 0) || ((game->ball.x + game->ball.w) >= SCREEN_WIDTH)) {
            game->vel_x *= -1;
        }

        if ((game->ball.y <= 0) || ((game->ball.y + game->ball.h) >= SCREEN_HEIGHT)) {
            game->vel_y *= -1;
        }

        // Game over check
        if (game->ball.y + game->ball.h >= SCREEN_HEIGHT) {
            game->state = PP_STATE_GAME_OVER;
        }
    }
    }
    return 0;
}

int event_game(PingPongGame *game, enum GameEvent event) {
    switch (game->state) {
    case PP_STATE_RUNNING: {
        switch (event) {
        case GE_KEY_LEFT: {
            game->pedal.x = min(SCREEN_WIDTH - PEDAL_WIDTH,
                                max(0, game->pedal.x - 2));
        } break;
        case GE_KEY_RIGHT: {
            game->pedal.x = min(SCREEN_WIDTH - PEDAL_WIDTH,
                                max(0, game->pedal.x + 2));
        } break;
        default:
            break;
        }
    } break;
    case PP_STATE_START:
    case PP_STATE_GAME_OVER: {
        if (event == GE_KEY_ENTER) {
            game_initialize(game);
            game->state = PP_STATE_RUNNING;
        }
    } break;
    }

    return 0;
}

int render_game(PingPongGame *game, SDL_Surface *screen, nSDL_Font *font) {
    switch (game->state) {
    case PP_STATE_GAME_OVER: {
        SDL_FillRect(screen, NULL, COLOR_BLACK);

        size_t messages_n = ARRAY_SIZE(game_over_messages);

        for (size_t i = 0; i < messages_n; ++i) {
            int message_width = nSDL_GetStringWidth(font, game_over_messages[i]);
            nSDL_DrawString(screen, font,
                            (SCREEN_WIDTH - message_width) * 0.5,
                            SCREEN_HEIGHT * 0.5 + 30 * ((int)i - 1),
                            game_over_messages[i]);
        }

        char score_buffer[64];
        if (sprintf(score_buffer, "Your score: %u", game->score) < 0) {
            return -1;
        }

        size_t score_width = nSDL_GetStringWidth(font, score_buffer);
        nSDL_DrawString(screen, font,
                        (SCREEN_WIDTH - score_width) * 0.5,
                        SCREEN_HEIGHT - 30,
                        score_buffer);

    } break;
    case PP_STATE_RUNNING: {
        SDL_FillRect(screen, NULL, COLOR_BLACK);
        SDL_FillRect(screen, &game->ball, COLOR_WHITE);
        SDL_FillRect(screen, &game->pedal, COLOR_WHITE);

        // Draw the current score
        char message_buf[32];
        if (sprintf(message_buf, "Score: %u", game->score) < 0) {
            return -1;
        }

        size_t message_width = nSDL_GetStringWidth(font, message_buf);

        nSDL_DrawString(screen, font,
                        (SCREEN_WIDTH - message_width - 3),
                        3, message_buf);
    } break;
    case PP_STATE_START: {
        SDL_FillRect(screen, NULL, COLOR_BLACK);

        size_t messages_n = ARRAY_SIZE(start_screen_messages);

        for (size_t i = 0; i < messages_n; ++i) {
            int message_width = nSDL_GetStringWidth(font, start_screen_messages[i]);
            nSDL_DrawString(screen, font,
                            (SCREEN_WIDTH - message_width) * 0.5,
                            SCREEN_HEIGHT * 0.5 + 30 * ((int)i - 1),
                            start_screen_messages[i]);
        }
    } break;
    }

    SDL_Flip(screen);

    return 0;
}
