/* Copyright 2020 Nico Sonack
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "pingpong.h"

// Ndless headers
#include <libndls.h>

// Newlib headers
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// SDL headers
#include <SDL/SDL.h>

static Uint32 fps = 20;

int main(void) {
    assert_ndless_rev(801);

    srand(time(NULL));

    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        fprintf(stderr, "Could not initialize SDL: %s\n", SDL_GetError());
        return EXIT_FAILURE;
    }

    SDL_Surface *screen = SDL_SetVideoMode(SCREEN_WIDTH,
                                           SCREEN_HEIGHT,
                                           has_colors ? 16 : 8,
                                           SDL_SWSURFACE);

    Uint32 dt = 1 / fps;
    PingPongGame *game = create_game();
    if (!game) {
        return EXIT_FAILURE;
    }

    nSDL_Font *font = nSDL_LoadFont(NSDL_FONT_VGA,
                                    COLOR_WHITE,
                                    COLOR_WHITE,
                                    COLOR_WHITE);
    if (!font) {
        destroy_game(game);
        SDL_Quit();
        return EXIT_FAILURE;
    }


    // Main game event loop
    for (;;) {
        Uint32 ticks_before = SDL_GetTicks();
        if (isKeyPressed(KEY_NSPIRE_LEFT)) {
            event_game(game, GE_KEY_LEFT);
        } else if (isKeyPressed(KEY_NSPIRE_RIGHT)) {
            event_game(game, GE_KEY_RIGHT);
        } else if (isKeyPressed(KEY_NSPIRE_ESC)) {
            break;
        } else if (isKeyPressed(KEY_NSPIRE_ENTER)) {
            event_game(game, GE_KEY_ENTER);
        }

        update_game(game, dt);
        render_game(game, screen, font);

        Uint32 ticks_after = SDL_GetTicks();

        if ((ticks_after - ticks_before) < dt) {
            SDL_Delay(dt - (ticks_after - ticks_before));
        }
    }

    destroy_game(game);
    nSDL_FreeFont(font);

    SDL_Quit();
    return 0;
}
