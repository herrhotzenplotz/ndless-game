/* Copyright 2020 Nico Sonack
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __PINGPONG_H
#define __PINGPONG_H1

#include <SDL/SDL.h>

#define COLOR_BLACK 0x00
#define COLOR_WHITE 0xff

enum PingPongGameState { PP_STATE_START, PP_STATE_RUNNING, PP_STATE_GAME_OVER };
enum GameEvent { GE_KEY_LEFT, GE_KEY_RIGHT, GE_KEY_ENTER };

struct PingPongGame {
    enum PingPongGameState state;
    SDL_Rect ball;
    SDL_Rect pedal;
    int vel_x, vel_y;
    unsigned score;
};

typedef struct PingPongGame PingPongGame;

PingPongGame *create_game();
void destroy_game(PingPongGame*);

int update_game(PingPongGame*, Uint32);
int event_game(PingPongGame*, enum GameEvent);
int render_game(PingPongGame*, SDL_Surface*, nSDL_Font*);

#endif
