# nspire-game

![](docs/pingpong.gif)

## Quick start

```bash
$ # bring ndless-sdk into your PATH
$ git clone https://gitlab.com/herrhotzenplotz/ndless-game
$ cd ndless-game/
$ make
$ # copy ndless-game.tns over to the calculator or emulator
```
## Binaries

Precompiled binaries can be found in the `binaries/` folder.  

## Requirements

- [Ndless](https://github.com/ndless-nspire/Ndless)

## License

See LICENSE
